package com.atlassian.bamboo.plugin.agentsmith.graphite;

import com.atlassian.aws.ec2.InstancePaymentType;
import com.atlassian.bamboo.plugin.agentsmith.AgentSmithService;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Agent;
import com.atlassian.bamboo.plugin.agentsmith.statistic.ElasticProperties;
import com.atlassian.bamboo.plugin.agentsmith.statistic.QueuedJob;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;
import com.atlassian.bamboo.plugin.graphiteconnector.GraphiteConnector;
import com.atlassian.bamboo.plugin.graphiteconnector.GraphiteConnectorBulkSender;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Status;
import static java.util.Map.Entry;

/**
 * Default implementation of the graphite service for agent Smith.
 * <p>
 * Automatically schedule an update with the {@link GraphiteUpdateJob}.
 * </p>
 */
public class GraphiteServiceImpl implements GraphiteService, LifecycleAware, DisposableBean {
    /**
     * Default interval between each execution of {@link GraphiteUpdateJob} in milliseconds.
     */
    private static final long DEFAULT_REPORT_INTERVAL = TimeUnit.SECONDS.toMillis(30);
    private static final Logger logger = LoggerFactory.getLogger(GraphiteServiceImpl.class);
    private static final String PATH_AGENTS = "agents";
    private static final String PATH_ELASTIC = PATH_AGENTS + ".elastic";
    private static final String LABEL_ELASTIC_DETACHED_VOLUMES = "ebsDetachedVolumes";
    private static final String LABEL_ELASTIC_SPOT_PRICE = "us_east.spot_price";
    private static final String LABEL_ELASTIC_TOTAL_PENDING_TIME = "totalPendingTime";
    private static final String LABEL_ELASTIC_MAX_PENDING_TIME = "maxPendingTime";
    private static final String LABEL_DEDICATED_AGENT = "dedicated";
    private static final String PATH_QUEUE = "queue";
    private static final String LABEL_QUEUE_SIZE = "size";
    private static final String LABEL_QUEUE_MAX_TIME = "maxTime";
    private static final String LABEL_QUEUE_TOTAL_TIME = "totalTime";
    private static final String LABEL_AGENT_TOTAL = "total";
    private final GraphiteConnector graphiteConnector;
    private final PluginScheduler pluginScheduler;
    private final AgentSmithService agentSmithService;
    private long interval = DEFAULT_REPORT_INTERVAL;

    /**
     * Creates a graphite service with its own scheduled update.
     *
     * @param graphiteConnector connection to the graphite server.
     * @param pluginScheduler   scheduler for automatic updates.
     * @param agentSmithService statistic gathering service.
     */
    public GraphiteServiceImpl(GraphiteConnector graphiteConnector, PluginScheduler pluginScheduler,
                               AgentSmithService agentSmithService) {
        this.graphiteConnector = graphiteConnector;
        this.pluginScheduler = pluginScheduler;
        this.agentSmithService = agentSmithService;
    }

    /**
     * Gets the name used by graphite to identify an agent type.
     *
     * @param agentType agent type to change into a String.
     * @return the string value of the agent type.
     */
    private static String getAgentTypeName(Agent.Type agentType) {
        switch (agentType) {
            case LOCAL:
                return "local";
            case REMOTE:
                return "remote";
            case ELASTIC:
                return "elastic";
            default:
                logger.error("Couldn't get the String version of '{}'", agentType);
                return "unknown";
        }
    }

    /**
     * Gets the name used by graphite to identify an agent status.
     *
     * @param status agent status to change into a String.
     * @return the string value of the agent status.
     */
    private static String getStatusName(Status status) {
        switch (status) {
            case IDLE:
                return "idle";
            case BUSY:
                return "busy";
            case HUNG:
                return "hung";
            case DISABLED:
                return "disabled";
            case OFFLINE:
                return "offline";
            default:
                logger.error("Couldn't get the String version of '{}'", status);
                return "unknown";
        }
    }

    /**
     * Gets the name used by graphite to identify an elastic agent status.
     *
     * @param elasticStatus elastic agent status to change into a String.
     * @return the string value of the elastic agent status.
     */
    private static String getElasticStatusName(ElasticProperties.ElasticStatus elasticStatus) {
        switch (elasticStatus) {
            case PENDING:
                return "pending";
            case PENDING_NO_INSTANCE:
                return "pendingNoInstance";
            case FAILED:
                return "failed";
            case SHUTTING_DOWN:
                return "shutting";
            case RUNNING:
                return "running";
            case OFFLINE:
                return "offline";
            case DISCONNECTED:
                return "disconnected";
            default:
                logger.error("Couldn't get the String version of '{}'", elasticStatus);
                return "unknown";
        }
    }

    /**
     * Gets the name used by graphite to identify an elastic agent payment type.
     *
     * @param instancePaymentType elastic agent payment type to change into a String.
     * @return the string value of the elastic agent payment type.
     */
    private static String getPaymentTypeName(InstancePaymentType instancePaymentType) {
        switch (instancePaymentType) {
            case REGULAR:
                return "regular";
            case SPOT:
                return "spot";
            case RESERVED:
                return "reserved";
            default:
                logger.error("Couldn't get the String version of '{}'", instancePaymentType);
                return "unknown";
        }
    }

    @Override
    public void onStart() {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(GraphiteUpdateJob.GRAPHITE_SERVICE_KEY, this);
        parameters.put(GraphiteUpdateJob.AGENT_SMITH_SERVICE_KEY, agentSmithService);
        pluginScheduler.scheduleJob(GraphiteUpdateJob.JOB_NAME, GraphiteUpdateJob.class,
                parameters, new Date(), interval);
        logger.info("Reporting job has ben scheduled to run every {}ms, starting now.", interval);
    }

    @Override
    public void destroy() throws Exception {
        try {
            pluginScheduler.unscheduleJob(GraphiteUpdateJob.JOB_NAME);
        } catch (IllegalArgumentException e) {
            logger.error("The job '{}' wasn't scheduled.", GraphiteUpdateJob.JOB_NAME, e);
        }
    }

    @Override
    public void reportToGraphite(Statistics statistics) {
        logger.info("Sending the current metrics to Graphite.");
        GraphiteConnectorBulkSender bulkSender = graphiteConnector.getBulkSender();

        sendAgentsDetails(bulkSender, statistics);
        sendElasticAgentsDetails(bulkSender, statistics);
        sendEbsDetails(bulkSender, statistics);
        sendQueueDetails(bulkSender, statistics);

        int sentCount = bulkSender.send();
        logger.debug("Successfully sent {} metrics to Graphite.", sentCount);
    }

    private void sendAgentsDetails(GraphiteConnectorBulkSender bulkSender, Statistics statistics) {
        long timestamp = TimeUnit.MILLISECONDS.toSeconds(statistics.getTimestamp());

        Map<Agent.Type, Statistics.Count<Status>> statusCountPerAgentType = statistics.getStatusCountPerAgentType();
        for (Entry<Agent.Type, Statistics.Count<Status>> typeEntry : statusCountPerAgentType.entrySet()) {
            String path = PATH_AGENTS + "." + getAgentTypeName(typeEntry.getKey());
            long totalAgents = 0L;
            for (Entry<Status, Long> statusEntry : typeEntry.getValue()) {
                long numberOfAgents = statusEntry.getValue();
                totalAgents += numberOfAgents;
                bulkSender.write(path, getStatusName(statusEntry.getKey()), Long.toString(numberOfAgents), timestamp);
            }
            bulkSender.write(path, LABEL_AGENT_TOTAL, Long.toString(totalAgents), timestamp);
        }

        for(Entry<Agent.Type, Long> entry : statistics.getDedicatedAgentCount()) {
            String path = PATH_AGENTS + "." + getAgentTypeName(entry.getKey());
            bulkSender.write(path, LABEL_DEDICATED_AGENT, entry.getValue().toString(), timestamp);
        }
    }

    private void sendElasticAgentsDetails(GraphiteConnectorBulkSender bulkSender, Statistics statistics) {
        long timestamp = TimeUnit.MILLISECONDS.toSeconds(statistics.getTimestamp());

        for (Entry<ElasticProperties.ElasticStatus, Long> elasticStatusEntry : statistics.getElasticStatusCount()) {
            bulkSender.write(PATH_ELASTIC, getElasticStatusName(elasticStatusEntry.getKey()),
                    Long.toString(elasticStatusEntry.getValue()), timestamp);
        }

        Map<InstancePaymentType, Statistics.Count<Status>> statusCountPerPaymentType =
                statistics.getStatusCountPerPaymentType();
        for (Entry<InstancePaymentType, Statistics.Count<Status>> typeEntry : statusCountPerPaymentType.entrySet()) {
            String path = PATH_ELASTIC + "." + getPaymentTypeName(typeEntry.getKey());
            long totalAgents = 0L;
            for (Entry<Status, Long> statusEntry : typeEntry.getValue()) {
                long numberOfAgents = statusEntry.getValue();
                totalAgents += numberOfAgents;
                bulkSender.write(path, getStatusName(statusEntry.getKey()), Long.toString(numberOfAgents), timestamp);
            }
            bulkSender.write(path, LABEL_AGENT_TOTAL, Long.toString(totalAgents), timestamp);
        }
        bulkSender.write(PATH_ELASTIC, LABEL_ELASTIC_TOTAL_PENDING_TIME,
                Long.toString(statistics.getElasticTotalWaitingTime()), timestamp);
        bulkSender.write(PATH_ELASTIC, LABEL_ELASTIC_MAX_PENDING_TIME,
                Long.toString(statistics.getElasticMaxWaitingTime()), timestamp);
        bulkSender.write(PATH_ELASTIC, LABEL_ELASTIC_SPOT_PRICE,
                Double.toString(statistics.getTotalCost()), timestamp);
    }

    private void sendEbsDetails(GraphiteConnectorBulkSender bulkSender, Statistics statistics) {
        long timestamp = TimeUnit.MILLISECONDS.toSeconds(statistics.getTimestamp());

        bulkSender.write(PATH_ELASTIC, LABEL_ELASTIC_DETACHED_VOLUMES,
                Long.toString(statistics.getDetachedVolumes()), timestamp);
    }

    private void sendQueueDetails(GraphiteConnectorBulkSender bulkSender, Statistics statistics) {
        long timestamp = TimeUnit.MILLISECONDS.toSeconds(statistics.getTimestamp());

        List<QueuedJob> queuedJobs = statistics.getQueuedJobs();
        bulkSender.write(PATH_QUEUE, LABEL_QUEUE_SIZE, Integer.toString(queuedJobs.size()), timestamp);
        Date longestQueuedDate = statistics.getLongestQueuedDate();
        long maxTime = longestQueuedDate != null ? TimeUnit.MILLISECONDS.toSeconds(longestQueuedDate.getTime()) : 0;
        bulkSender.write(PATH_QUEUE, LABEL_QUEUE_MAX_TIME, Long.toString(maxTime), timestamp);
        long totalQueueTime = statistics.getTotalQueueTime();
        bulkSender.write(PATH_QUEUE, LABEL_QUEUE_TOTAL_TIME, Long.toString(totalQueueTime), timestamp);
    }

    /**
     * Gets the interval between each graphite update (in milliseconds).
     *
     * @return the interval between each graphite update.
     */
    public long getInterval() {
        return interval;
    }

    /**
     * Sets the interval between each graphite update (in milliseconds).
     *
     * @param interval interval between each graphite update (in milliseconds).
     */
    public void setInterval(long interval) {
        this.interval = interval;
    }
}
