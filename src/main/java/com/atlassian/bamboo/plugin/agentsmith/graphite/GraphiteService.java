package com.atlassian.bamboo.plugin.agentsmith.graphite;

import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;

/**
 * Service in charge of sending agent Smith statistics to graphite.
 */
public interface GraphiteService {
    /**
     * Send the current report to graphite.
     *
     * @param statistics statistics to send to graphite.
     */
    void reportToGraphite(Statistics statistics);
}
