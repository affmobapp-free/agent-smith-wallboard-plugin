package com.atlassian.bamboo.plugin.agentsmith;

import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Volume;
import com.amazonaws.services.ec2.model.VolumeAttachment;
import com.atlassian.aws.AWSAccount;
import com.atlassian.bamboo.agent.elastic.aws.AwsAccountBean;
import com.atlassian.bamboo.agent.elastic.server.ElasticAccountManagementService;
import com.atlassian.bamboo.agent.elastic.server.ElasticInstanceManager;
import com.atlassian.bamboo.build.BuildExecutionManager;
import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;
import com.atlassian.bamboo.v2.build.queue.BuildQueueManager;
import com.atlassian.bamboo.ww2.actions.admin.elastic.ElasticUIBean;
import com.google.common.collect.ImmutableList;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class StatisticsServiceDetachedVolumesTest {
    private AgentSmithServiceImpl statisticsService;
    @Mocked
    private AwsAccountBean mockAwsAccountBean;
    @Mocked
    private AWSAccount mockAwsAccount;
    @Mocked
    private ElasticAccountManagementService elasticAccountManagementService;
    @Mocked
    private AgentManager mockAgentManager;
    @Mocked
    private ElasticInstanceManager mockElasticInstanceManager;
    @Mocked
    private BuildExecutionManager mockBuildExecutionManager;
    @Mocked
    private ElasticUIBean mockElasticUIBean;
    @Mocked
    private BuildQueueManager mockBuildQueueManager;
    @Injectable
    private Volume volume1;
    @Injectable
    private Volume volume2;
    @Injectable
    private Volume volume3;
    @Injectable
    private VolumeAttachment volumeAttachment;

    @BeforeMethod
    public void setUp() throws Exception {
        new NonStrictExpectations() {

            {
                mockAwsAccountBean.getAwsAccount();
                result = mockAwsAccount;
                mockAwsAccount.getAllInstances();
                result = Collections.<Instance>emptySet();
                mockAgentManager.getAllLocalAgents();
                result = Collections.emptyList();
                mockAgentManager.getAllRemoteAgents();
                result = Collections.emptyList();
                mockAgentManager.getOnlineElasticAgents();
                result = Collections.emptyList();

                mockBuildQueueManager.getBuildQueue();
                result = ImmutableList.of();

                statisticsService = new AgentSmithServiceImpl(mockAgentManager, mockElasticInstanceManager,
                        mockBuildExecutionManager, mockBuildQueueManager, mockElasticUIBean, mockAwsAccountBean, elasticAccountManagementService);
            }
        };

    }

    @Test

    public void testDetachedVolumesReturnsZeroWhenAwsNotConnected() throws Exception {
        new NonStrictExpectations() {{
            mockAwsAccountBean.getAwsAccount();
            result = null;
        }};

        Statistics statistics = statisticsService.getStatistics();

        assertThat(statistics.getDetachedVolumes(), is(0L));
    }

    @Test
    public void testDetachedVolumesProperlySet() throws Exception {
        new Expectations() {

            {
                mockAwsAccount.describeVolumes();
                result = Arrays.asList(volume1, volume2, volume3);
                volume1.getAttachments();
                result = Collections.singletonList(volumeAttachment);
                volume2.getAttachments().isEmpty();
                result = Collections.emptyList();
                volume3.getAttachments().isEmpty();
                result = Collections.singletonList(volumeAttachment);
            }
        };

        Statistics statistics = statisticsService.getStatistics();

        assertThat(statistics.getDetachedVolumes(), is(1L));
    }
}
